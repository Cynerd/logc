/* SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright 2021-2022, CZ.NIC z.s.p.o. (http://www.nic.cz/)
 * Copyright (c) 2022 Karel Kočí <cynerd@email.cz>
 */
#include <unistd.h>

struct fakesyslog {
	int priority;
	char *msg;
};

extern struct fakesyslog *fakesyslog;
extern size_t fakesyslog_cnt;

void fakesyslog_reset();
void fakesyslog_free();
